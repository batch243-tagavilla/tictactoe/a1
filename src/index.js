import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";

function Square(props) {
    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    );
}

function Board(props) {
    const renderSquare = (i) => {
        return (
            <Square value={props.board[i]} onClick={() => props.onClick(i)} />
        );
    };

    return (
        <div>
            <div className="board-row">
                {renderSquare(0)}
                {renderSquare(1)}
                {renderSquare(2)}
            </div>
            <div className="board-row">
                {renderSquare(3)}
                {renderSquare(4)}
                {renderSquare(5)}
            </div>
            <div className="board-row">
                {renderSquare(6)}
                {renderSquare(7)}
                {renderSquare(8)}
            </div>
        </div>
    );
}

function Game() {
    const [board, setBoard] = useState(Array(9).fill(null));
    const [showBoard, setShowBoard] = useState(false);
    const [player, setPlayer] = useState(`X`);
    const [status, setStatus] = useState(``);

    useEffect(() => {
        setStatus(`Current player: ${player}`);
    }, [player]);

    const clickSquare = (i) => {
        const tempArr = board.slice();

        if (!tempArr[i] && !checkWinner(tempArr)) {
            tempArr[i] = player;
            setBoard(tempArr);

            if (checkWinner(tempArr)) {
                setStatus(`Winner: Player ${player}`);
                document.querySelector(`#startBtn`).removeAttribute(`disabled`);
                return;
            }

            setPlayer(player === `X` ? `O` : `X`);
        }
    };

    const startGame = () => {
        setStatus(`Current player: ${player}`);
        setPlayer(`X`);
        setBoard(Array(9).fill(null));
        setShowBoard(true);
        document.querySelector(`#startBtn`).setAttribute(`disabled`, true);
    };

    return (
        <div id="main">
            <button id="startBtn" onClick={() => startGame()}>
                New Game
            </button>
            {showBoard && (
                <div className="game">
                    <div className="game-board">
                        <Board
                            board={board}
                            onClick={(i) => {
                                clickSquare(i);
                            }}
                        />
                    </div>
                    <div className="game-info">
                        <div className="status">{status}</div>
                        <ol>{/* TODO */}</ol>
                    </div>
                </div>
            )}
        </div>
    );
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Game />);

function checkWinner(board) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];

        if (board[a] && board[a] === board[b] && board[a] === board[c]) {
            return board[a];
        }
    }

    return null;
}

function compChoice(board) {
    const validMoves = [];
    let choice = 0;

    if (!board[4]) {
        choice = 4;
        console.log(`AI move: ${choice}`);
        return choice;
    } else {
        for (let i = 0; i<board.length; i++) {
            if (!board[i]) {
                validMoves.push(i);
            }
        }

        choice = Math.floor(Math.random() * (validMoves.length));
        console.log(`AI move: ${choice}`);
        return choice;
    }
}